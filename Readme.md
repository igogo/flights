**Install**
```cmd
    npm i
```

**Run dev**
```cmd
    npm start
```

**Tests**
```cmd
    npm test
```

**Build**
```cmd
    npm build
```