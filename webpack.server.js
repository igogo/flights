const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const config = require('./webpack.config')
const path = require('path')
const app = require('express')()

const port = 3003

const compiler = webpack(config)
app.use(webpackDevMiddleware(compiler, {
  noInfo: true,
  lazy: true,
  publicPath: config.output.publicPath
}))

app.use(webpackHotMiddleware(compiler))

app.get('/api/flights', function (req, res) {
  const data = require(`${__dirname}/data.json`)
  res.send(data)
})

app.get('/dist/*/', function (req, res) {
  const filename = path.normalize(path.join(__dirname, req.path))
  res.sendFile(filename)
})

app.use(function (req, res) {
  res.sendFile(`${__dirname}/index.html`)
})

app.listen(port, function (error) {
  if (error) {
    console.error(error)
  } else {
    console.info('==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port)
  }
})
