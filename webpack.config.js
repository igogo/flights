const path = require('path')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')
const NpmInstallPlugin = require('npm-install-webpack-plugin')

const plugins = [
  new webpack.optimize.OccurrenceOrderPlugin(),
  new NpmInstallPlugin(),
  new webpack.DefinePlugin({
    'process.env': JSON.stringify({
      NODE_ENV: process.env.NODE_ENV
    })
  })
]

let entry = [
  './node_modules/es5-shim/es5-shim',
  './node_modules/es5-shim/es5-sham',
  './node_modules/promise/index',
  './node_modules/whatwg-fetch/fetch',
  'babel-polyfill',
  './src/index'
]

if (process.env.NODE_ENV === 'development') {
  plugins.push(new webpack.HotModuleReplacementPlugin())

  entry = [
    'webpack-hot-middleware/client',
    './node_modules/es5-shim/es5-shim',
    './node_modules/es5-shim/es5-sham',
    './node_modules/promise/index',
    './node_modules/whatwg-fetch/fetch',
    'babel-polyfill',
    './src/index'
  ]
} else {
  plugins.push(new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false
    }
  }))
}

module.exports = {
  devtool: process.env.NODE_ENV === 'development' ? 'cheap-eval-source-map' : 'source-map',
  entry: entry,
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/dist/',
    pathinfo: true
  },
  plugins: plugins,
  postcss: function () {
    return [autoprefixer({
      browsers: ['last 3 versions']
    })]
  },
  module: {
    preLoaders: [{
      test: /\.js$/,
      loaders: ['eslint'],
      include: [
        path.resolve(__dirname, 'src')
      ]
    }],
    loaders: [{
      test: /\.js$/,
      loaders: ['babel'],
      include: path.join(__dirname, 'src')
    }, {
      test: /\.css$/,
      loaders: ['style', 'css']
    }, {
      test: /\.scss$/,
      loaders: ['style', 'css', 'postcss', 'sass?sourceMap&root=./dist/img/']
    }, {
      test: /\.woff$/,
      loader: 'url-loader?limit=10000&mimetype=application/font-woff&name=[path][name].[ext]'
    }, {
      test: /\.woff2$/,
      loader: 'url-loader?limit=10000&mimetype=application/font-woff2&name=[path][name].[ext]'
    }, {
      test: /\.(jpeg|json|png|jpg|woff|woff2|eot|ttf|svg)(\?.*$|$)/,
      loader: 'file'
    }, {
      test: /\.json$/,
      loader: 'json-loader?limit=10000&mimetype=application/json&name=[path][name].[ext]'
    }]
  }
}
