import React from 'react'

const styles = {
  item: {
    padding: 20,
    border: '1px solid #ccc',
    flexBasis: 250,
    flex: 'flex-grow',
    marginRight: 20,
    marginBottom: 20
  },
  p: {
    margin: 0
  }
}

export default class FlightItem extends React.Component {
  static propTypes = {
    direction: React.PropTypes.object.isRequired,
    arrival: React.PropTypes.string.isRequired,
    departure: React.PropTypes.string.isRequired,
    carrier: React.PropTypes.string.isRequired
  }

  static defaultProps = {
    direction: {}
  }

  render () {
    const {direction, arrival, departure, carrier} = this.props

    return (
      <div style={styles.item} className='flight-item'>
        <p style={styles.p} className='flight-item__direction-from'><b>Откуда:</b> {direction.from}</p>
        <p style={styles.p} className='flight-item__direction-to'><b>Куда:</b> {direction.to}</p>
        <p style={styles.p} className='flight-item__departure'><b>Время вылета:</b> {departure}</p>
        <p style={styles.p} className='flight-item__arrival'><b>Время прилета:</b> {arrival}</p>
        <p style={styles.p} className='flight-item__carrier'><b>Название авиакомпании:</b> {carrier}</p>
      </div>
    )
  }
}
