import React from 'react'
import './loader.scss'

const Loader = () => (
  <div className='loader-wrap'>
    <div className='uil-ring-css' style={{transform: 'scale(0.24)'}}>
      <div />
    </div>
  </div>
)

export default Loader
