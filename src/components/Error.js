import React from 'react'
import './error.scss'

const Error = (props) => (
  props.message ? (
    <div className='error'>
      <b>Error:</b> {props.message}
    </div>
  ) : null
)

export default Error
