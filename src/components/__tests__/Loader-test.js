import React from 'react'
import Loader from '../Loader'
import { shallow } from 'enzyme'

test('<Loader />', () => {
  const component = shallow(
    <Loader />
  )

  expect(component.find('.loader-wrap').length).toBe(1)
  expect(component.find('.loader-wrap > .uil-ring-css').length).toBe(1)
  expect(component.find('.loader-wrap > .uil-ring-css > div').length).toBe(1)
})
