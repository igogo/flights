import React from 'react'
import Error from '../Error'
import faker from 'faker'
import { shallow } from 'enzyme'

test('<Error />', () => {
  const message = faker.lorem.sentence()
  const component = shallow(
    <Error message={message} />
  )

  expect(component.text()).toEqual(`Error: ${message}`)

  const emptyComponent = shallow(<Error />)

  expect(emptyComponent.text()).toEqual('')
})
