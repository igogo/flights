import React from 'react'
import FlightItem from '../FlightItem'
import { shallow } from 'enzyme'
import moment from 'moment'
import faker from 'faker'
import {config} from '../../config'

test('<FlightItem />', () => {
  const props = {
    direction: {
      from: faker.address.city(),
      to: faker.address.city()
    },
    arrival: moment(faker.date.future()).format(config.DATE_FORMAT),
    departure: moment(faker.date.future()).format(config.DATE_FORMAT),
    carrier: faker.company.companyName()
  }

  const component = shallow(
    <FlightItem {...props} />
  )

  expect(component.find('.flight-item__direction-from').text()).toEqual(`Откуда: ${props.direction.from}`)
  expect(component.find('.flight-item__direction-to').text()).toEqual(`Куда: ${props.direction.to}`)
  expect(component.find('.flight-item__departure').text()).toEqual(`Время вылета: ${props.departure}`)
  expect(component.find('.flight-item__arrival').text()).toEqual(`Время прилета: ${props.arrival}`)
  expect(component.find('.flight-item__carrier').text()).toEqual(`Название авиакомпании: ${props.carrier}`)
})
