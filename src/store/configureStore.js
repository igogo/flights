import {createStore, applyMiddleware} from 'redux'
import rootReducer from '../reducers'
import {syncHistoryWithStore, routerMiddleware} from 'react-router-redux'
import thunkMiddleware from 'redux-thunk'
import reduxCatch from 'redux-catch'
import {config} from '../config'

const errorHandler = (error) => {
  // Logging here!
  console.error('Error', error)
}

export default (baseHistory, initialState) => {
  const routingMiddleware = routerMiddleware(baseHistory)
  const middlewares = [routingMiddleware, thunkMiddleware, reduxCatch(errorHandler)]

  if (config.DEBUG) {
    const createLogger = require('redux-logger')
    const logger = createLogger()
    middlewares.push(logger)
  }

  const store = createStore(rootReducer, initialState, applyMiddleware(...middlewares))
  const history = syncHistoryWithStore(baseHistory, store)

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers')
      store.replaceReducer(nextRootReducer)
    })
  }

  return {history, store}
}
