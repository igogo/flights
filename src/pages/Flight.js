import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import FlightItem from '../components/FlightItem'
import {getAll, filter, resetRoute} from '../actions/flightActions'
import {getFilteredFlights, contains} from '../utils'
import Loader from '../components/Loader'
import Error from '../components/Error'

const styles = {
  container: {
    width: '80%',
    margin: '0 auto',
    padding: 20
  },
  select: {
    display: 'block',
    height: 40,
    width: '100%',
    marginBottom: 20
  },
  flightContainer: {
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'flex-start'
  }
}

@connect(state => ({
  app: state.app,
  carrier: state.carrier,
  flight: state.flight
}), dispatch => ({
  actions: bindActionCreators({getAll, filter, resetRoute}, dispatch)
}))
export default class Flight extends React.Component {
  componentDidMount () {
    this.props.actions.getAll(this.props.params.carrier)
  }

  changeCarrierHandler = (e) => {
    this.props.actions.filter(e.target.value)
  }

  componentWillReceiveProps (nextProps) {
    const {params, actions} = this.props
    const {carrier} = nextProps
    const nextParamsCarrier = nextProps.params.carrier

    if (params.carrier !== nextParamsCarrier && !contains(carrier.items, nextParamsCarrier)) {
      actions.resetRoute()
    }
  }

  renderPage () {
    const {carrier, flight} = this.props
    const showLoading = carrier.loading || flight.loading

    return showLoading ? <Loader /> : (
      <div>
        <select
          style={styles.select}
          value={flight.selectedCarrier || ''}
          placeholder='Выберите авиакомпанию'
          onChange={this.changeCarrierHandler}
        >
          <option value=''>Все авиакомании</option>
          {carrier.items.map((item, index) => <option key={`carrier-${index}`} value={item}>{item}</option>)}
        </select>
        <div style={styles.flightContainer}>
          {getFilteredFlights(flight.items, flight.selectedCarrier).map((item, index) =>
            <FlightItem key={`flight-${index}`} {...item} />
          )}
        </div>
      </div>
    )
  }

  render () {
    const {app} = this.props

    return (
      <div style={styles.container}>
        <h1 style={styles.title}>Перелеты</h1>
        {app.error ? <Error message={app.error} /> : this.renderPage()}
      </div>
    )
  }
}
