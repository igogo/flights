import keyMirror from 'keymirror'

export const config = {
  API_URL: 'http://localhost:3003',
  DEBUG: process.env.NODE_ENV === 'development',
  DATE_FORMAT: 'YYYY-MM-DD hh:mm'
}

export const ActionTypes = keyMirror({

  FLIGHT_START: null,
  FLIGHT_COMPLETE: null,

  REQUEST_ERROR: null,

  CARRIER_START: null,
  CARRIER_COMPLETE: null

})
