import React from 'react'
import {Route} from 'react-router'
import Flight from '../pages/Flight'

export default (
  <Route path='/' component={Flight}>
    <Route path=':carrier' component={Flight} />
  </Route>
)
