// @flow
import { config } from '../config'
import fetch from 'isomorphic-fetch'

export default (url) => fetch(`${config.API_URL}${url}`)
  .then((response) => {
    if (!response.ok) {
      const error = new Error(response.statusText)
      error.response = response
      throw error
    }

    return response.json()
  })
