// @flow
import moment from 'moment'
import {config} from '../config'
import api from './api'

const createReducer = (initialState:Object, reducerMap:Object):Function =>
(state:Object = initialState, action:Object):Object => {
  if (typeof reducerMap[action.type] === 'function') {
    return reducerMap[action.type](state, action.payload)
  }

  return state
}

const contains = (arr:string[], item:?string = null):boolean => !item || arr.indexOf(item) !== -1

const prepareData = (data:Object):Object => {
  const carriers = []
  const flights = data.flights.map(item => {
    if (carriers.indexOf(item.carrier) === -1) {
      carriers.push(item.carrier)
    }

    item.arrival = moment(item.arrival).format(config.DATE_FORMAT)
    item.departure = moment(item.departure).format(config.DATE_FORMAT)

    return item
  })

  return {flights, carriers}
}

const getFilteredFlights = (flights:Object, carrier:?string = null) => {
  if (!carrier) {
    return flights
  }

  return flights.filter(flight => flight.carrier === carrier)
}

export {
  api,
  createReducer,
  contains,
  prepareData,
  getFilteredFlights
}
