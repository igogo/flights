import React from 'react'
import ReactDOM from 'react-dom'
import configureStore from './store/configureStore'
import Root from './containers/Root'
import {browserHistory} from 'react-router'

const {store, history} = configureStore(browserHistory, {})

ReactDOM.render((
  <Root store={store} history={history} />
  ), document.getElementById('app-root')
)
