import reducer, { initialState } from '../appReducer'
import { ActionTypes } from '../../config'

describe('app reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual(initialState)
  })

  it('should handle REQUEST_ERROR', () => {
    expect(
      reducer(undefined, {
        type: ActionTypes.REQUEST_ERROR,
        payload: 'Test Error Message'
      })
    ).toEqual({
      ...initialState,
      error: 'Test Error Message'
    })
  })
})
