import reducer, { initialState } from '../carrierReducer'
import { ActionTypes } from '../../config'
import faker from 'faker'

describe('carrier reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual(initialState)
  })

  it('should handle CARRIER_START', () => {
    expect(
      reducer(undefined, {
        type: ActionTypes.CARRIER_START
      })
    ).toEqual({
      ...initialState,
      loading: true
    })
  })

  it('should handle REQUEST_ERROR', () => {
    expect(
      reducer(undefined, {
        type: ActionTypes.REQUEST_ERROR
      })
    ).toEqual({
      ...initialState,
      loading: false
    })
  })

  it('should handle CARRIER_COMPLETE', () => {
    const items = [
      faker.company.companyName(),
      faker.company.companyName(),
      faker.company.companyName()
    ]

    expect(
      reducer(undefined, {
        type: ActionTypes.CARRIER_COMPLETE,
        payload: items
      })
    ).toEqual({
      ...initialState,
      loading: false,
      items
    })
  })
})
