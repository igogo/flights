import reducer, { initialState } from '../flightReducer'
import { ActionTypes } from '../../config'
import faker from 'faker'

describe('flight reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual(initialState)
  })

  it('should handle FLIGHT_START', () => {
    expect(
      reducer(undefined, {
        type: ActionTypes.FLIGHT_START
      })
    ).toEqual({
      ...initialState,
      loading: true
    })
  })

  it('should handle REQUEST_ERROR', () => {
    expect(
      reducer(undefined, {
        type: ActionTypes.REQUEST_ERROR
      })
    ).toEqual({
      ...initialState,
      loading: false
    })
  })

  it('should handle FLIGHT_COMPLETE', () => {
    const corrier1 = faker.company.companyName()
    const corrier2 = faker.company.companyName()
    const data = {
      flights: [
        {
          id: faker.random.number(),
          direction: {
            from: faker.address.city(),
            to: faker.address.city()
          },
          arrival: faker.date.future(),
          departure: faker.date.future(),
          carrier: corrier1
        },
        {
          id: faker.random.number(),
          direction: {
            from: faker.address.city(),
            to: faker.address.city()
          },
          arrival: faker.date.future(),
          departure: faker.date.future(),
          carrier: corrier2
        }
      ]
    }
    const payload = {
      selectedCarrier: corrier1,
      items: data.flights
    }

    expect(
      reducer(undefined, {
        type: ActionTypes.FLIGHT_COMPLETE,
        payload
      })
    ).toEqual({
      ...initialState,
      ...payload,
      loading: false
    })
  })
})
