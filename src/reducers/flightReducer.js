// @flow
import {ActionTypes} from '../config'
import {createReducer} from '../utils'
import {LOCATION_CHANGE} from 'react-router-redux'

export type FlightType = {
  items:Array<Object>,
  loading:boolean,
  selectedCarrier:?string
}

export const initialState:FlightType = {
  items: [],
  loading: false,
  selectedCarrier: null
}

export default createReducer(initialState, {
  [ActionTypes.FLIGHT_START]: (state):FlightType => ({
    ...state,
    loading: true
  }),

  [ActionTypes.REQUEST_ERROR]: (state):FlightType => ({
    ...state,
    loading: false
  }),

  [ActionTypes.FLIGHT_COMPLETE]: (state, payload):FlightType => ({
    ...state,
    ...payload,
    loading: false
  }),

  [LOCATION_CHANGE]: (state, payload):FlightType => ({
    ...state,
    selectedCarrier: payload.pathname.replace(/\//g, ''),
    loading: false
  })
})
