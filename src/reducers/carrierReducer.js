// @flow
import { ActionTypes } from '../config'
import { createReducer } from '../utils'

export type CarrierType = {
  items:string[],
  loading:boolean
}

export const initialState:CarrierType = {
  items: [],
  loading: false
}

export default createReducer(initialState, {
  [ActionTypes.CARRIER_START]: (state):CarrierType => ({
    ...state,
    loading: true
  }),

  [ActionTypes.REQUEST_ERROR]: (state):CarrierType => ({
    ...state,
    loading: false
  }),

  [ActionTypes.CARRIER_COMPLETE]: (state, payload):CarrierType => ({
    ...state,
    items: payload,
    loading: false
  })
})
