import {combineReducers} from 'redux'
import carrier from './carrierReducer'
import flight from './flightReducer'
import app from './appReducer'
import { routerReducer as routing } from 'react-router-redux'

const appReducer = combineReducers({carrier, flight, routing, app})

export default (state, action) => appReducer(state, action)
