// @flow
import {ActionTypes} from '../config'
import {createReducer} from '../utils'

export type AppType = {
  error:?string
}

export const initialState:AppType = {
  error: null
}

export default createReducer(initialState, {
  [ActionTypes.REQUEST_ERROR]: (state, payload):AppType => ({
    ...state,
    error: payload
  })
})
