// @flow
import { browserHistory } from 'react-router'
import { ActionTypes } from '../config'
import * as carrierActions from './carrierActions'
import { prepareData, contains, api } from '../utils'

const startRequest = () => ({
  type: ActionTypes.FLIGHT_START
})

const requestError = (error) => ({
  type: ActionTypes.REQUEST_ERROR,
  payload: error
})

const resolveAll = (items:Object, selectedCarrier:?string) => ({
  type: ActionTypes.FLIGHT_COMPLETE,
  payload: {
    items,
    selectedCarrier
  }
})

export const resetRoute = () => {
  browserHistory.push('/')
}

export const filter = (selectedCarrier:string) => (dispatch:Function, getState:Function) => {
  dispatch(startRequest())

  const {carrier, flight} = getState()
  if (contains(carrier.items, selectedCarrier)) {
    browserHistory.push(`/${selectedCarrier}`)
    return false
  }

  resetRoute()
  dispatch(resolveAll(flight.items, null))
}

export const getAll = (selectedCarrier:?string = null) => async (dispatch:Function) => {
  dispatch(startRequest())
  dispatch(carrierActions.startRequest())

  return api('/api/flights').then((data) => {
    const {flights, carriers} = prepareData(data)
    let _selectedCarrier = selectedCarrier

    if (!contains(carriers, selectedCarrier)) {
      resetRoute()
      _selectedCarrier = null
    }

    dispatch(resolveAll(flights, _selectedCarrier))
    dispatch(carrierActions.resolveAll(carriers))
  }).catch((error) => {
    dispatch(requestError(error.message))
  })
}
