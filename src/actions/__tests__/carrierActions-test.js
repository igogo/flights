import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../carrierActions'
import { ActionTypes } from '../../config'
import { initialState } from '../../reducers/carrierReducer'
import faker from 'faker'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Carrier actions', () => {
  it('creates CARRIER_COMPLETE when carrier has been loaded', () => {
    const data = [
      faker.lorem.word(),
      faker.lorem.word(),
      faker.lorem.word()
    ]

    const expectedActions = [
      {
        type: ActionTypes.CARRIER_COMPLETE,
        payload: data
      }
    ]

    const store = mockStore({carrier: {...initialState}})

    store.dispatch(actions.resolveAll(data))
    return expect(store.getActions()).toEqual(expectedActions)
  })

  it('creates CARRIER_START when loading has been started', () => {
    const expectedActions = [
      {
        type: ActionTypes.CARRIER_START
      }
    ]

    const store = mockStore({carrier: {...initialState}})

    store.dispatch(actions.startRequest())
    return expect(store.getActions()).toEqual(expectedActions)
  })
})
