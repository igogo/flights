import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../flightActions'
import { ActionTypes, config } from '../../config'
import { initialState } from '../../reducers/flightReducer'
import { initialState as carrierInitialState } from '../../reducers/carrierReducer'
import nock from 'nock'
import faker from 'faker'
import moment from 'moment'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Flight actions', () => {
  afterEach(() => {
    nock.cleanAll()
  })

  it('creates FLIGHT_COMPLETE when flight has been loaded', async () => {
    const corrier1 = faker.company.companyName()
    const corrier2 = faker.company.companyName()
    const data = {
      flights: [
        {
          id: faker.random.number(),
          direction: {
            from: faker.address.city(),
            to: faker.address.city()
          },
          arrival: faker.date.future(),
          departure: faker.date.future(),
          carrier: corrier1
        },
        {
          id: faker.random.number(),
          direction: {
            from: faker.address.city(),
            to: faker.address.city()
          },
          arrival: faker.date.future(),
          departure: faker.date.future(),
          carrier: corrier2
        }
      ]
    }

    nock(config.API_URL)
      .get('/api/flights')
      .reply(200, data)

    const expectedActions = [
      {
        type: ActionTypes.FLIGHT_START
      },
      {
        type: ActionTypes.CARRIER_START
      },
      {
        type: ActionTypes.FLIGHT_COMPLETE,
        payload: {
          items: data.flights.map((item) => ({
            ...item,
            arrival: moment(item.arrival).format(config.DATE_FORMAT),
            departure: moment(item.departure).format(config.DATE_FORMAT)
          })),
          selectedCarrier: null
        }
      },
      {
        type: ActionTypes.CARRIER_COMPLETE,
        payload: [corrier1, corrier2]
      }
    ]

    const store = mockStore({
      flight: {...initialState},
      carrier: {...carrierInitialState}
    })

    return store.dispatch(actions.getAll()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('creates REQUEST_ERROR when flight has not been loaded', async () => {
    const message = 'Internal Server Error'

    nock(config.API_URL)
      .get('/api/flights')
      .reply(500)

    const expectedActions = [
      {
        type: ActionTypes.FLIGHT_START
      },
      {
        type: ActionTypes.CARRIER_START
      },
      {
        type: ActionTypes.REQUEST_ERROR,
        payload: message
      }
    ]

    const store = mockStore({
      flight: {...initialState},
      carrier: {...carrierInitialState}
    })

    return store.dispatch(actions.getAll()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
