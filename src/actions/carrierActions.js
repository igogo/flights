import { ActionTypes } from '../config'

export const startRequest = () => ({
  type: ActionTypes.CARRIER_START
})

export const resolveAll = (data:string[]) => ({
  type: ActionTypes.CARRIER_COMPLETE,
  payload: data
})
